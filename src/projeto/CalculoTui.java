package projeto;

import java.util.Scanner;

//Classe que serve como interface 
public class CalculoTui {
	private static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("Digite numero para obter fatorial:");
		int valorCalculado = scan.nextInt();
		if (valorCalculado < 0) {
			System.out.print("N�o existe fatorial para nmeros negativos!");
			System	.out.close();
		} else {
			int fat = Calculo.calculoFatorial(valorCalculado);
			System.out.println("O fatorial de " + valorCalculado + "  igual a " + fat);
		}
	}

}